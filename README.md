# Server
These instructions are to help you set up a server for all of the projects that the IoT team has. This will require a little bit of cooperation with the DevOps team as without access to Azure, there's not a whole lot you can do. They will more than likely have some sort of ticketing process so feel free to talk to them about it. 

This will deploy a production server used to serve the public. There's still a lot more that needs to be done outside of these instructions however this will serve as a start and a much more straight forward explanation of how it is all set up.

## Table of Contents
- [Preparation](#prep)
- [Set up](#setup)
    * [Docker](#docker)
    * [Docker-Compose](#compose)
    * [Finishing Up](#finish)
- [Installing the Projects/reverse proxy](#projects)
- [Locking up](#lockup)
- [Common Issues](#issues)
<a name="prep"></a>
## Preparation
These are the specifications of our current server. Chances are you'll need something around the same specs else you may end up having throttling issues.
- 3GB RAM
- 30GB Storage
- Ports 80, 443
- Ubuntu 20.04 (Preferably, any distro will probably work)
<a name="setup"></a>
## Set up
Once you have your server up and running, you'll have to do a couple of things first.
Firstly, you'll need to ensure the system is up to date.
```bash
sudo apt update
sudo apt upgrade
```
<a name="docker"></a>
### Docker
Once that's been taken care of, we need to install docker, docker-compose and all of their depenencies through their repository rather than through Ubuntu's. Let's first install the depenencies.
```bash
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```
Next we add the GPG key so we can get our packages signed.
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
Now we just add the repository for docker to our package manager.
```bash
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```
Simply install docker through apt.
```bash
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```
And to see if it's installed.
```bash
docker --version
```
[Docker Installation Instructions](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository)
<a name="compose"></a>
### Docker Compose
To install docker-compose, we need to do it through a different method. We'll have to curl it from their github repo. This command will curl version 1.27.4 into our binary directory but feel free to look up what the latest version and subsitute whatever the current version is.
```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```
Once that's done, we'll need to change the permissions.
```bash
sudo chmod +x /usr/local/bin/docker-compose
```

Now use the following command to test to see if it works.
```bash
docker-compose --version
```
[Docker Compose Installation Instructions](https://docs.docker.com/compose/install/)
<a name="finish"></a>
### Finishing up
Finally, chances are you'll have nginx and apache running in the background of the server. We'll need those services disabled so we can use those ports and close off another vector of attack just in case.
```bash
sudo systemctl disable nginx.service
sudo systemctl disable apache.service
```

<a name="projects"></a>
## Installing the Projects/reverse proxy
Currently, we are running four different projects across twelve containers. Every single project should have their own readmes however, if there isn't chances are they will follow the same installation instructions. But please consult their indivdual readme/deployment files. They should be at their repostiory's root or in their container directory. Some projects may not have docker-compose files ready. If that is the case, check if they have a branch called "dockerify".

However, for the reverse proxy you will have to make your own configuration files for the proxy. Each project should have their own configuration file in the includes directory of the reverse proxy. Then in the default.conf in the root of the container, they should be included into it like the following:
```bash
include includes/<config>.conf;
```
That will include them into the proxy when it's built. An example will be provided to you as well as the existing files we use for our system. The `server_name` should be the URL for where the service will go while the `proxy_pass` is the name of the container (as that is it's hostname as well in the system) followed up by whichever port they use. It defaults to port 80.

Once that is done and every other project that uses the reverse proxy is running first, you may build and run the reverse proxy. Running the proxy first will result in it failing as it cannot find the hostnames to serve those applications.
```bash
docker-compose build
docker-compose up -d
docker-compose logs # To check if it failed or not
```

If there's no errors, your server should be running swimmingly. However, chances are it won't be for various reasons.

<a name="lockup"></a>
## Locking The Server Up
Now all of our projects are in place, we can look into locking down the server. There are two options we can go down here. One is passwordless logins using ssh keys or we can use indivdual users with one time passwords. For this, we'll simply stick with the former as the latter includes a bigger overhaul to how we use our server. However, it's something that might be neccessersary in the future. To compliment this, we also use Fail2Ban which automatically bans any IP who fails autheticiation three times in a row. The latter is not as neccessersary if you're using passwordless logins however it's a nice thing to have.

Generating and adding an SSH key to authorized_keys is quite easy and simple to do. I'll leave the instructions for how to do it per platform down below.

[Windows/PuTTY](https://www.ssh.com/ssh/putty/windows/puttygen)

Simply generate a key, append the public key to `~/.ssh/authorized_keys` and then you sign in using that key either via `ssh -i <keypath> <user>@<ip>` or through PuTTY with `Connection > SSH > Auth > private key for auth`.

Now that's done, just edit `/etc/ssh/sshd_config` and change the line `PasswordAuthentication yes` to `no`.

Lastly, we simply restart the SSH daemon and it'll now be unable to be accessed without the SSH key. Please verify that the key works before you make the switch though.

Now if you want to stop brute force attacks, I recommend using fail2ban. Install using the following command.
```bash
sudo apt install fail2ban
```
Then enable and start it with the next command.
```bash
sudo systemctl enable fail2ban.service && sudo systemctl start fail2ban.service
```
That will enable it to start on boot and start it immidately. Now if any user fails to login three consecutive times in a row, their ip will be banned. 

<a name="issues"></a>
## Common Issues
>Reverse Proxy is not found

Check if the hostname in the configuration is correct then check if any of the projects that use nginx to serve their websites are also properly configured. However, if nothing has been touched it should be working fine.

>Ports are in use

Check if apache and nginx that came with Ubuntu are still running. Else, check if any of the container's internal ports are also being used. Chances are, it'll be either of those problems. I highly recommend unless it's absolutely neccessersary to just leave the internal ports alone and just define the external ones like the following.
```yml
ports:
    - 80
```

>I need to unban someone from ssh!

Simply use the following command.
```bash
sudo fail2ban-client sshd unbanip <IP here>
```

# Old Documentation

The GitLab repository for all server development documentation for the Dunedin IoT project group.

## The Things Network

The Things Network provides an MQTT broker that we use in order to recieve live information from our devices. Previously we were able to connect our web applications directly to our own MQTT broker provided by LoRa Server, however this is not possible
with TTN for security and cost reasons.

To solve this issue, we use a relay that recieves live data from TTN's MQTT server and republishes that information to a new topic on our own local MQTT server. This allows us to hide our access keys from public access and gives us more control over our own MQTT platform. 

### Instructions 

The following instructions assume you have established a brand new Ubuntu VM. Run the following commands in order to run the script. 

```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install python
sudo apt-get install git
sudo apt-get install python-pip
sudo pip install paho-mqtt

git clone https://gitlab.com/iotop/server.git (no longer https://gitlab.op-bit.nz/BIT/Project/Internet-Of-Things/server.git)
cd server
```

At this point you'll need to edit the `relay.py` script using your editor of choice, add application credentials and the topic you wish to send data to on the local MQTT broker. 

Once you've made the required edits, you can run the script with the following command:

```
python relay.py
```

// TODO: Write install script to run on boot.