# How to use the Reverse Proxy
NOTE: Dev deployment instructions are at the bottom.

## Prerequisites
You must have the following installed:

- Docker
- Docker-compose (NOTE: You must install this seperately) 

You need to know how to deploy docker web servers for example we use will the Orokonui web app and ingest (check Ingest & Orokonui repos).

## What is a Reverse Proxy?
In a proxy the server does not know about the client as the proxy intercepts the requests and then communicates with the server. 
However, in a reverse proxy the client does not know the servers. So in our situation the client would go to IOT (Internet Of Things) only then does the reverse proxy
intercept, the reverse proxy sends out the request to the origin server and then recieves a response from them.

![Diagram Image](Images/DiagramV2.PNG)

## Instructions (Linux version)
NOTE: This code references "~/docker/reverse". Two options A: Change that to your location or B: Create the same directories so you can just copy and paste basic commands.

1. Install nginx
>sudo apt-install nginx

2. Clone and pull down all the web servers required
    The reverse proxy, Orokonui web app and ingest must be under the same level. 

![directory Image](Images/ls.PNG)

    A) Reverse proxy (https://gitlab.com/iotop/server)

    Once cloned move the '/nginxProxy' directory to be inside '/reverse' and remove the rest of the server repo.
        
        sudo mv ~/docker/reverse/server/nginxProxy/ ~/docker/reverse/
        cd ~/docker/reverse
        sudo rm - r server/
    
    B) Orokonui Ingest (https://gitlab.com/iotop/ingest)

    Once cloned move the the '/web_app' directory to be inside '/reverse' and remove the rest of the ingest repo.
        
        sudo mv ~/docker/reverse/ingest/web_app/ ~/docker/reverse/
        cd ~/docker/reverse
        sudo rm - r ingest/

    C-Part 1) Orokonui web app (https://gitlab.com/iotop/orokonui)

    Once cloned move the '/project' directory to be inside '/reverse' and remove the rest of the ingest repo.
    
    NOTE: You need both /Project including 'Project/Project', so move '/Project' **not** only just 'Project/Project' with is just the folder within.    
        
        sudo mv ~/docker/reverse/orokonui/orokonui-web-app/Projectenv/Project ~/docker/reverse/
        cd ~/docker/reverse
        sudo rm - r orokonui/
        
    C-Part 2) Sensitive (https://gitlab.com/iotop/sensitive/)

    Orokonui requires private files. The file 'secrets.py' contains a secret key and .env file (hidden file) has the db credentials. 
    
    To show the .env file use:
    > ls -a
    
    NOTE: Move these files into '/Project/Project' and **not** in '/Project'
        
        sudo mv ~/docker/reverse/sensitive/Oronokui/.env ~/docker/reverse/
        sudo mv ~/docker/reverse/sensitive/Oronokui/secrets.py ~/docker/reverse/
        cd ~/docker/reverse
        sudo rm - r sensitive/
        
3. Modify the hosts
    Now me must add the ip addresses and translate them into a domain name.
    
    We need to first get our address using ifconfig. If you don't have ifconfig install the following:
    >sudo apt-get install net-tools
    
    Now run:
    >ifconfig
    
    Use the ip address shown for ens33: 

    ![ifconfig](Images/ens33.PNG)

    Make the changes look simlar within the hosts file to the picture shown below:
    >sudo vim /etc/hosts
    
    ![adding hosts](Images/hosts.PNG)
    
4. As the ice rink web app in not fully complete, go into '/nginx/default.conf' and comment out like shown in this picture:

   ![commenting ice rink](Images/comment.PNG)
    
5. We will now build, run and test
    
    NOTE: The reverse proxy must go last, otherwise you will get an upstream error.

        cd ~/docker/reverse/web_app
        docker-compose build --no-cache
        docker-compose up -d
        
        cd ~/docker/reverse/Project
        docker-compose build --no-cache
        docker-compose up -d
        
        cd ~/docker/reverse/nginxProxy
        docker-compose build --no-cache
        docker-compose up -d
        
    The '--no-cache' extenstion build without cache.
    The '-d' extenstion runs the container in the background.
    
    We will now use the domains we created earlier. Open your browser and type what is highlighted in the picture:
    
    ![display sites](Images/domains.PNG)
    
## Toubleshooting
![stop server](Images/systemctl.PNG)

If this error occurs type these commands then repeat step 5.
>systemctl stop nginx.service

>systemctl stop apache2.service

This error occurs if the ports are alrady in use by another service.
    
If your getting other issues the first thing to do is check you are running all the following containers:

![show running containers](Images/dockerps.PNG)

Run 'docker-compose up' without '-d' to get more details.

## Deploying to Dev server
To access the dev server, you will need to putty into it using information from the sensitive repo (https://gitlab.com/iotop/sensitive/).

1. Add the ip addresses and the domain to the hosts on your physical machine. (Picture bellow shows file location)
![show running containers](Images/DevHosts.PNG)

NOTE: The ip address is the Dev servers and the translated domain name that will be put in the browser is next to it.

2. Repeat step 5. You browser should look the same as the picture in that step.
