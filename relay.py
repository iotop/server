# TTN MQTT Relay 
# Purpose: Relay MQTT messages from The Things Network to a local MQTT broker

import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish

TTN_BROKER = '52.62.83.250'	# TTN Application Handler
TTN_USERNAME = 'op_roomsensors'					# TTN Application Name
TTN_PASSWORD = 'FILL ME IN' 	# TTN Access Key
TTN_TOPIC = '+/devices/+/up'			# Subscribe to all incoming messages

LOCAL_BROKER = '10.118.2.129'	# Local broker running on LoRa Server
LOCAL_TOPIC = 'ttn/op_roomsensors' 	# Topic name for all application data
LOCAL_PORT = 1883				# Port for local MQTT broker 

def on_connect(client, userdata, flags, rc):
    """Subscribe to topic after connection to broker is made."""
    print("Connected with result code", str(rc))
    client.subscribe(TTN_TOPIC)


def on_message(client, userdata, msg):
    """Relay message to a different broker."""
    publish.single(
        LOCAL_TOPIC, payload=msg.payload, qos=0, retain=False,
        hostname=LOCAL_BROKER, port=LOCAL_PORT, client_id='ttn-local',
        keepalive=60, will=None, auth=None, tls=None, protocol=mqtt.MQTTv311)


client = mqtt.Client()
client.username_pw_set(TTN_USERNAME, password=TTN_PASSWORD)
client.on_connect = on_connect
client.on_message = on_message
client.connect(TTN_BROKER, LOCAL_PORT, 60)

client.loop_forever()