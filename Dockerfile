FROM python:3

ADD relay.py /

RUN pip install paho-mqtt

CMD [ "python", "./relay.py" ]
